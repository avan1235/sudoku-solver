public class SudokuConstants {
    public static final int BLOCK_SIZE = 3;
    public static final int NUMBERS = BLOCK_SIZE*BLOCK_SIZE;
    public static final int MAP_SIZE = 3;
    public static final int BLOCKS_ON_MAP = NUMBERS;

}
