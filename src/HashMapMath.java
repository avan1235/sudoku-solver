import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HashMapMath {
    public static HashMap<Integer,Boolean> normalizeHashMapToFalse(HashMap<Integer, Boolean> map){
        for (int i = 1; i <= SudokuConstants.NUMBERS; i++) {
            if (!map.containsKey(i)){
                map.put(i, false);
            }
        }
        return map;
    }

    public static HashMap<Integer,Boolean> normalizeHashMapToTrue(HashMap<Integer, Boolean> map){
        for (int i = 1; i <= SudokuConstants.NUMBERS; i++) {
            if (!map.containsKey(i)){
                map.put(i, true);
            }
        }
        return map;
    }

    public static HashMap<Integer, Boolean> conjunctionOfMaps( HashMap<Integer, Boolean> map1,  HashMap<Integer, Boolean> map2){
        HashMap <Integer, Boolean> res = new HashMap<>();
        res = normalizeHashMapToFalse(res);
        for (int key : res.keySet()){
            res.put(key, (map1.get(key) && map2.get(key)));
        }
        return res;
    }

    public static HashMap<Integer, Boolean> alternativeOfMaps(HashMap<Integer, Boolean> map1,  HashMap<Integer, Boolean> map2){
        HashMap <Integer, Boolean> res = new HashMap<>();
        res = normalizeHashMapToFalse(res);
        for (int key : res.keySet()){
            res.put(key, (map1.get(key) || map2.get(key)));
        }
        return res;
    }



    public static HashMap<Integer, Boolean> negationOfMap( HashMap<Integer, Boolean> map){
        HashMap <Integer, Boolean> res = new HashMap<>();
        res = normalizeHashMapToTrue(res);
        for (int key : res.keySet()){
           if (map.get(key)){
               res.put(key, false);
           }
        }
        return res;
    }

    public static int[] hashMapToList(HashMap<Integer, Boolean> map){
        List<Integer> ret = new ArrayList();

        for (Integer num : map.keySet()) {
            if (map.get(num)){
                ret.add(num);
            }
        }

        int[] res = ret.stream().mapToInt(i -> i).toArray();
        return res;
    }
}
