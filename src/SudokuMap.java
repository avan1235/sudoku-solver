import java.util.HashMap;

public class SudokuMap extends SudokuPart {
    SudokuBlock[] blocks = new SudokuBlock[SudokuConstants.BLOCKS_ON_MAP];

    {
        for (int i = 0; i < SudokuConstants.BLOCKS_ON_MAP; i++){
            blocks[i] = new SudokuBlock();
        }
    }


    public SudokuMap(){
        // default constructor
    }

    // copying th map object
    public SudokuMap(SudokuMap map){
        this.blocks = map.blocks;
    }

    public boolean hasValue(int xLocal, int yLocal){
        try {
            int v = this.getValue(xLocal, yLocal);
            return true;
        } catch (Exception e){
            return false;
        }
    }


    public HashMap<Integer, Boolean> getBlockValues(int xLocal, int yLocal){
        return this.getProperBlock(xLocal, yLocal).getBlockValues();
    }

    public HashMap<Integer, Boolean> getColumnValues(int col){
        SudokuBlock[] blocks = getProperBlocksOfColums(col);
        HashMap <Integer, Boolean> map = new HashMap<>();
        map = HashMapMath.normalizeHashMapToFalse(map);
        col = (col - 1) % SudokuConstants.MAP_SIZE + 1;
        for (SudokuBlock block : blocks) {
            map = HashMapMath.alternativeOfMaps(map, block.getColumnValues(col));
        }
        return map;
    }

    public HashMap<Integer, Boolean> getRowValues(int row){
        SudokuBlock[] blocks = getProperBlocksOfRows(row);
        HashMap<Integer, Boolean> map = new HashMap<>();
        map = HashMapMath.normalizeHashMapToFalse(map);
        row = (row - 1) % SudokuConstants.MAP_SIZE + 1;
        for (SudokuBlock block : blocks){
            map = HashMapMath.alternativeOfMaps(map, block.getRowValues(row));
        }
        return map;
    }


    public int[] getBlockListValues(int xLocal, int yLocal){
        return this.getProperBlock(xLocal, yLocal).getBlockListValues();
    }

    public int[] getColumnListValues(int col){
        return  HashMapMath.hashMapToList(this.getColumnValues(col));
    }

    public int[] getRowListValues(int row){
        return HashMapMath.hashMapToList(this.getRowValues(row));
    }


    /*
     * coordinates of block has to be given so the number from 1 to BLOCK_SIZE
     */
    private SudokuBlock getProperBlock(int xLocal, int yLocal){
        if (checkCoordinates(SudokuConstants.MAP_SIZE, xLocal, yLocal)) {
            return this.blocks[blockIndex(xLocal, yLocal, SudokuConstants.BLOCK_SIZE)];
        }
        else{
            badNumberException("Bad coordinate of SudokuBlock given in map.");
            return new SudokuBlock();
        }
    }

    private SudokuBlock[] getProperBlocksOfColums(int col){
        SudokuBlock[] ret = new SudokuBlock[SudokuConstants.MAP_SIZE];

        if (checkCoordinates(SudokuConstants.NUMBERS, col)) {
            for (int i = 0; i < SudokuConstants.MAP_SIZE; i++){
                ret[i] = getProperBlock((col - 1) / 3 + 1, (i*SudokuConstants.MAP_SIZE) / 3 + 1);
            }
            return ret;
        }
        else{
            badNumberException("Bad coordinate of SudokuBlock given in map.");
            return new SudokuBlock[0];
        }
    }

    private SudokuBlock[] getProperBlocksOfRows(int row){
        SudokuBlock[] ret = new SudokuBlock[SudokuConstants.MAP_SIZE];

        if (checkCoordinates(SudokuConstants.NUMBERS, row)) {
            for (int i = 0; i < SudokuConstants.MAP_SIZE; i++){
                ret[i] = getProperBlock((i*SudokuConstants.MAP_SIZE) / 3 + 1, (row - 1) / 3 + 1);
            }
            return ret;
        }
        else{
            badNumberException("Bad coordinate of SudokuBlock given in map.");
            return new SudokuBlock[0];
        }
    }

    public void setValue(int xLocal, int yLocal, int value){
        if (checkCoordinates(SudokuConstants.NUMBERS, xLocal, yLocal)){
            int index = getBlockNumberOnMap(xLocal, yLocal);
            this.blocks[index].setValue(
                                (xLocal - 1) % SudokuConstants.MAP_SIZE + 1,
                                (yLocal - 1) % SudokuConstants.MAP_SIZE + 1,
                                        value);

        }
        else {
            badNumberException("Bad coordinate of SudokuField on map given in parameter");
        }
    }

    public void setPreditions(int xLocal, int yLocal, HashMap<Integer, Boolean> preditions){
        if (checkCoordinates(SudokuConstants.NUMBERS, xLocal, yLocal)){
            int index = getBlockNumberOnMap(xLocal, yLocal);
            this.blocks[index].setPredictionsAt(
                    (xLocal - 1) % SudokuConstants.MAP_SIZE + 1,
                    (yLocal - 1) % SudokuConstants.MAP_SIZE + 1,
                    preditions);

        }
        else {
            badNumberException("Bad coordinate of SudokuField on map given in parameter");
        }
    }

    public int getValue(int xLocal, int yLocal){
        if (checkCoordinates(SudokuConstants.NUMBERS, xLocal, yLocal)){
            return this.blocks[getBlockNumberOnMap(xLocal, yLocal)].getValue(
                    (xLocal - 1) % SudokuConstants.MAP_SIZE + 1,
                    (yLocal - 1) % SudokuConstants.MAP_SIZE + 1
            );
        }
        else{
            badNumberException("Bad coordinate of SudokuField on map given in parameter");
            return -1;
        }
    }

    public HashMap<Integer, Boolean> getPredictions(int xLocal, int yLocal){
        if (checkCoordinates(SudokuConstants.NUMBERS, xLocal, yLocal)){
            return this.blocks[getBlockNumberOnMap(xLocal, yLocal)].getPreditionsAt(
                    (xLocal - 1) % SudokuConstants.MAP_SIZE + 1,
                    (yLocal - 1) % SudokuConstants.MAP_SIZE + 1
            );
        }
        else{
            badNumberException("Bad coordinate of SudokuField on map given in parameter");
            return new HashMap<>();
        }
    }

    public String toString(){
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < SudokuConstants.NUMBERS; i++) {
            builder = builder.append(" ---");
        }

        for (int i = 1; i <= SudokuConstants.NUMBERS; i++){
            builder = builder.append("\n|");
            for (int j = 1; j <= SudokuConstants.NUMBERS; j++) {
                String value;
                try {
                    value = Integer.toString(getValue(j, i));
                } catch (Exception e){
                    value = " ";
                }
                builder = builder.append(" ").append(value).append(" |");
            }
            builder = builder.append("\n");
            for (int k = 0; k < SudokuConstants.NUMBERS; k++) {
                builder = builder.append(" ---");
            }
        }
        return  builder.toString();
    }


    private int getBlockNumberOnMap(int x, int y){
        return blockIndex(
                (x - 1) / SudokuConstants.MAP_SIZE + 1,
                (y - 1) / SudokuConstants.MAP_SIZE + 1,
                SudokuConstants.MAP_SIZE);
    }
}
