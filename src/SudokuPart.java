public class SudokuPart {

    protected boolean checkCoordinates(int size, int... cords){
        for (int cord : cords) {
            if ( !(cord >= 1 && cord <= size)) {
                return false;
            }
        }
        return true;
    }

    /*
     * the data x and y has to be given in range 1 to max_n
     */
    protected int blockIndex(int x, int y, int size){
        return x - 1 + size * (y - 1);
    }


    protected void badNumberException(String error){
        throw new NumberFormatException(error);
    }
}
