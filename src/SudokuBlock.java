import java.util.HashMap;

public class SudokuBlock extends SudokuPart{

    SudokuField[] fields = new SudokuField[SudokuConstants.BLOCK_SIZE * SudokuConstants.BLOCK_SIZE];

    // creating an empty block using zero size predicts tables
    {
        for (int i = 0; i < SudokuConstants.BLOCK_SIZE * SudokuConstants.BLOCK_SIZE; i++){
            fields[i] = new SudokuField(new int[0]);
        }
    }


    // default constructor
    public SudokuBlock(){

    }

    /*
     * setValue gets the x and y coordinates of field to be set
     * xLocal and yLocal should be the numbers from 1 to SudokuConstants.BLOCK_SIZE
     * for bad coordinates throws NumberFormatException
     */
    public void setValue(int xLocal, int yLocal, int value){
        if (checkCoordinates(SudokuConstants.BLOCK_SIZE, xLocal, yLocal)) {
            this.fields[blockIndex(xLocal, yLocal, SudokuConstants.BLOCK_SIZE)].setValue(value);
        }
        else {
            badNumberException("Bad coordinate of SudokuField given in block.");
        }
    }


    public int getValue(int xLocal, int yLocal){
        if (checkCoordinates(SudokuConstants.BLOCK_SIZE, xLocal, yLocal)){
            return this.fields[blockIndex(xLocal, yLocal, SudokuConstants.BLOCK_SIZE)].getValue();
        }
        else {
            badNumberException("Bad coordinate of SudokuField given in block.");
            return -1;
        }
    }

    public HashMap<Integer, Boolean> getBlockValues(){
        HashMap<Integer, Boolean> ret = new HashMap<>();
        for (SudokuField field : fields){
            try {
                ret.put(field.getValue(), true);
            }
            catch (NullPointerException npe){
                // don't take number in this case
            }
        }

        // adding this numbers which doesn't occur in fields
        return HashMapMath.normalizeHashMapToFalse(ret);
    }

    public HashMap<Integer, Boolean> getColumnValues(int col){
        HashMap<Integer, Boolean> ret = new HashMap<>();
        if (checkCoordinates(SudokuConstants.BLOCK_SIZE, col)){
            col -= 1;
            for (int i = 0; i < SudokuConstants.BLOCK_SIZE; i++){
                try {
                    ret.put(this.fields[col+i*SudokuConstants.BLOCK_SIZE].getValue(), true);
                }
                catch (NullPointerException npe){
                    // don't add this number
                }
            }
            return HashMapMath.normalizeHashMapToFalse(ret);
        }
        else{
            badNumberException("Bad coordinate of column given");
            return new HashMap<>();
        }
    }

    public HashMap<Integer, Boolean> getRowValues(int row){
        HashMap<Integer, Boolean> ret = new HashMap<>();

        if (checkCoordinates(SudokuConstants.BLOCK_SIZE, row)){
            row -= 1;
            for (int i = 0; i < SudokuConstants.BLOCK_SIZE; i++){
                try {

                    ret.put(this.fields[row*SudokuConstants.BLOCK_SIZE+i].getValue(), true);
                }
                catch (NullPointerException npe){
                    // don't add this number
                }
            }
            return HashMapMath.normalizeHashMapToFalse(ret);
        }
        else{
            badNumberException("Bad coordinate of column given");
            return new HashMap<>();
        }
    }



    public int[] getColumnListValues(int col){
        return HashMapMath.hashMapToList(getColumnValues(col));
    }

    public int[] getRowListValues(int row){
        return HashMapMath.hashMapToList(getRowValues(row));
    }


    public int[] getBlockListValues(){
        return HashMapMath.hashMapToList(getBlockValues());
    }


    public void setPredictionsAt(int xLocal, int yLocal, HashMap<Integer, Boolean> values){
        if (checkCoordinates(SudokuConstants.BLOCK_SIZE, xLocal, yLocal)){
            for (int key : values.keySet()){
                if (values.get(key)){
                    this.fields[blockIndex(xLocal, yLocal, SudokuConstants.BLOCK_SIZE)].setPredictions(key, true);
                }
            }
        }
    }

    public HashMap<Integer, Boolean> getPreditionsAt(int xLocal, int yLocal){
        if (checkCoordinates(SudokuConstants.BLOCK_SIZE, xLocal, yLocal)){
            return this.fields[blockIndex(xLocal, yLocal, SudokuConstants.BLOCK_SIZE)].getPredictions();
        }
        else {
            badNumberException("Bad coordinate of SudokuField given in block.");
            return new HashMap<>();
        }
    }


    public String toString(){
        StringBuilder ret = new StringBuilder("");
        ret.append(" --- --- --- \n");
        for (int i = 1; i <= SudokuConstants.BLOCK_SIZE; i++) {
            ret.append("| ");
            for (int j = 1; j <= SudokuConstants.BLOCK_SIZE; j++){
                ret.append(this.fields[blockIndex(j, i, SudokuConstants.BLOCK_SIZE)].getStringValue());
                ret.append(" | ");
            }
            ret.append("\n");
            ret.append(" --- --- --- \n");
        }

        return ret.toString();
    }
}
