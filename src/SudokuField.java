import java.util.HashMap;

public class SudokuField {
    private SudokuNumber value = null;
    private HashMap<Integer, Boolean> predictions;

    {
        predictions = new HashMap<>(SudokuConstants.NUMBERS);
        for (int val : SudokuNumber.values){
            predictions.put(new SudokuNumber(val).getIntNumber(), false);
        }
    }

    public SudokuField(int val){
        value = new SudokuNumber(val);
    }

    public SudokuField(int[] predicts){
        for (int val : predicts){
            predictions.put(new SudokuNumber(val).getIntNumber(), true);
        }
    }

    public HashMap<Integer, Boolean> getPredictions() {
        return predictions;
    }

    public void setPredictions(int num, boolean pred){
        predictions.put(new SudokuNumber(num).getIntNumber(), pred);
    }

    public int getValue(){
        if (this.hasValue()){
            return this.value.getIntNumber();
        }
        else {
            throw new NullPointerException("Value number wasn't set yet, you cannot refer to it");
        }
    }

    public boolean hasValue(){
        if (value != null){
            return true;
        }
        else {
            return false;
        }
    }

    public void setValue(int value) {
        if (this.value != null){
            this.value.setVal(value);
        }
        else{
            this.value = new SudokuNumber(value);
        }
    }

    public String getStringValue(){
        if (value != null){
            return Integer.toString(this.value.getIntNumber());
        }
        else {
            return " ";
        }
    }

    public String toString(){
        String ret = "";
        if (value == null) {
            ret += "Predictions are:";
            for (int val : SudokuNumber.values){
                if (this.predictions.get(val)) {
                    ret += " " + val;
                }
            }
        }
        else {
            ret += "Field value is: " + value.toString();
        }
        return ret + "\n";
    }
}
