public class SudokuNumber {

    public final static int[] values = {1, 2, 3, 4, 5, 6, 7, 8, 9};


    private int val;


    public SudokuNumber(int val) {
        setProperValue(val);
    }


    public int getIntNumber() {
        return val;
    }


    public void setVal(int val){
        setProperValue(val);
    }

    private void setProperValue(int val){
        if (val >= 0 && val <= 9) {
            this.val = val;
        }
        else {
            throw new NumberFormatException("Bad number given");
        }
    }

    public String toString(){
        return Integer.toString(val);
    }
}
