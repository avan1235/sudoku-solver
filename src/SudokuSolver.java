import java.util.Arrays;
import java.util.HashMap;

public class SudokuSolver {
    private SudokuMap initialMap;
    private SudokuMap map;
    private int emptyFields;

    public SudokuSolver(SudokuMap map){
        this.initialMap = map;
        this.map = new SudokuMap(map);
        this.emptyFields = scanEmptyFields(this.map);
        System.out.println(this.map);
        System.out.println(this.initialReasoning() ? "Solved" : "Partially solved");
        System.out.println(this.map);

    }

    private int scanEmptyFields(SudokuMap map){
        int result = SudokuConstants.NUMBERS * SudokuConstants.NUMBERS;
        for (int y = 1; y <= SudokuConstants.NUMBERS; y++) {
            for (int x = 1; x <= SudokuConstants.NUMBERS; x++) {
                if (map.hasValue(x,y)){
                    result -= 1;
                }
            }
        }
        return result;
    }

    private boolean setPreditionsOnMapAndWrite(){
        boolean anyChanged = false;
        for (int y = 1; y <= SudokuConstants.NUMBERS; y++) {
            for (int x = 1; x <= SudokuConstants.NUMBERS; x++) {
                if (!this.map.hasValue(x, y)){
                    HashMap<Integer, Boolean> rowValues = HashMapMath.negationOfMap(map.getRowValues(y));
                    HashMap<Integer, Boolean> columnValues = HashMapMath.negationOfMap(map.getColumnValues(x));
                    HashMap<Integer, Boolean> blockValues = HashMapMath.negationOfMap(map.getBlockValues(
                            (x-1) / SudokuConstants.BLOCK_SIZE + 1,
                            (y-1) / SudokuConstants.BLOCK_SIZE + 1));
                    HashMap<Integer, Boolean> predictions = HashMapMath.conjunctionOfMaps(
                            HashMapMath.conjunctionOfMaps(rowValues, columnValues), blockValues);

                    int howManyPredictions = 0;
                    for (int key : predictions.keySet()){
                        if (predictions.get(key)){
                            howManyPredictions += 1;
                        }
                    }

                    if (howManyPredictions == 1){
                        emptyFields -= 1;
                        anyChanged = true;
                        int[] predictsArray = HashMapMath.hashMapToList(predictions);
                        map.setValue(x, y, predictsArray[0]);
                    }
                    this.map.setPreditions(x, y, predictions);
                }
            }
        }
        return anyChanged;
    }

    private boolean initialReasoning(){
        while (setPreditionsOnMapAndWrite());
        if (this.emptyFields == 0){
            return true;
        }
        else{
            return false;
        }
    }

    private String allPreditionsToString(){
        StringBuilder builder = new StringBuilder();

        for (int y = 1; y <= SudokuConstants.NUMBERS; y++) {
            for (int x = 1; x <= SudokuConstants.NUMBERS; x++) {
                builder = builder.append("x=").append(x).append(" y=").append(y).append(" -> ").append(
                        Arrays.toString(HashMapMath.hashMapToList(map.getPredictions(x, y)))
                ).append("\n");
            }
        }
        return builder.toString();
    }
}
