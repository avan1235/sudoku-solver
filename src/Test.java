import java.util.Arrays;

public class Test {
    public static void main(String[] args){
        System.out.println("====================== FIELD TEST ======================");
        int[] pre = new int[3];
        pre[0] = 3;
        pre[1] = 5;
        pre[2] = 8;

        SudokuField sudokuField = new SudokuField(pre);
        System.out.printf(sudokuField.toString());

        System.out.println("====================== BLOCK TEST ======================");
        SudokuBlock block = new SudokuBlock();

        block.setValue(2, 3, 9);
        block.setValue(3,1,2);
        block.setValue(1,2,1);
        System.out.println(block.toString());

        System.out.println("====================== MAPS TEST ======================");
        SudokuMap map = new SudokuMap();
        map.setValue(2, 3, 5);
        map.setValue(3,1, 9);
        map.setValue(9, 2,3);
        map.setValue(5, 5, 1);
        map.setValue(9,9, 2);
        map.setValue(7,7,7);
        map.setValue(5,1,7);
        System.out.println(map.getBlockValues(3,3));
        System.out.println(Arrays.toString(map.getBlockListValues(3,3)));
        System.out.println(Arrays.toString(map.getColumnListValues(5)));
        System.out.println(Arrays.toString(map.getColumnListValues(9)));
        System.out.println(map.toString());
        System.out.println(Arrays.toString(map.getRowListValues(1)));
        System.out.println(Arrays.toString(map.getRowListValues(2)));

        System.out.println("==================== PREDICTIONS TEST ====================");
        SudokuMap mapSolving = new SudokuMap();
        mapSolving.setValue(4,1,1);
        mapSolving.setValue(6,1,5);
        mapSolving.setValue(8,1,6);
        mapSolving.setValue(9,1,8);
        mapSolving.setValue(7,2,7);
        mapSolving.setValue(9,2,1);
        mapSolving.setValue(1,3,9);
        mapSolving.setValue(3,3,1);
        mapSolving.setValue(8,3,3);
        mapSolving.setValue(3,4,7);
        mapSolving.setValue(5,4,2);
        mapSolving.setValue(6,4,6);
        mapSolving.setValue(1,5,5);
        mapSolving.setValue(9,5,3);
        mapSolving.setValue(4,6,8);
        mapSolving.setValue(5,6,7);
        mapSolving.setValue(7,6,4);
        mapSolving.setValue(2,7,3);
        mapSolving.setValue(7,7,8);
        mapSolving.setValue(9,7,5);
        mapSolving.setValue(1,8,1);
        mapSolving.setValue(3,8,5);
        mapSolving.setValue(1,9,7);
        mapSolving.setValue(2,9,9);
        mapSolving.setValue(4,9,4);
        mapSolving.setValue(6,9,1);
        SudokuSolver solver = new SudokuSolver(mapSolving);
    }
}
